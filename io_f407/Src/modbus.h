#ifndef __MBUS_H__
#define __MBUS_H__

#include "stm32f4xx.h"

void modbus_send_exception(void);
void modbus_send_response(void);
void modbus_on_message(void);

void modbus_response_error(uint8_t func_num, uint8_t error_code);
void modbus_response_echo(uint8_t* message);
void modbus_response(void);

void modbus_on_rtu(uint8_t* message, uint8_t size);
void modbus_init(void);

#endif // __MBUS_H__
