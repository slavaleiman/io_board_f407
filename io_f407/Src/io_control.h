#ifndef __IO_CONTROL_H__
#define __IO_CONTROL_H__

#include <stdbool.h>
#include "main.h"
/****************************************
********** MODBUS MESSAGE FORMAT ********
*****************************************/

// Запрос температуры
// addr :8
// size :8
// n    :16     // счетчик pd2
// wl   :16     //
// b    :8      // счетчик 0..255
// fzt  :8      // флаг запроса термометров
// zt[0]:8      // температура первого термометра
// ..
// zt[23]:8
// ft[0]:8      // флаг первого термометра
// ..
// ft[23]:8
// crc  :16

#define OUTPUT_PORT GPIOC
#define INPUT_PORT  GPIOB

//commands
void io_control_init(void);
void io_control_update_idr(void);
void io_control_update_tsensors(void);
void io_control_poll(void);             // poll changes
void io_control_on_i2c(void);           // response from i2c
void io_control_save_data(void);
//get
uint16_t io_control_idr(void);               // состояние дискретных входов
uint8_t* io_control_tsensor_addrs(void);     //
bool     io_control_tsensors_status(void);   // общее состояние линии i2c
uint16_t io_control_tsensor(uint8_t idx);    // значение температуры или флаг ошибки
uint16_t io_control_poll_params(void);       // параметры опроса
uint32_t io_control_impulse_counter(void);
uint32_t io_control_impulse_freq(void);

//set
void io_control_tsensor_set_addrs(uint8_t* message);
void io_control_set_gpio_output(uint16_t out);
void io_control_set_poll_params(uint16_t pop); // установка параметров опроса

void io_control_on_temp(uint8_t*);
void io_control_on_impulse(uint16_t imp);
#endif /*__IO_CONTROL_H__*/
