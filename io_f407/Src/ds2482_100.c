/*
    После запуска команды, нужно мониторить (poll) состояние флага busy,
    а затем проверять соответствующие флаги в status регистре.
*/

#include <stdbool.h>
#include "stm32f4xx.h"
#include "io_control.h"
#include "ds2482_100.h"
#include "tsensor.h"

extern I2C_HandleTypeDef hi2c1;
#define HI2C    hi2c1

#define DS2482_ADDR                 0x18

#define DS2482_DEVICE_RESET         0xF0
#define DS2482_SET_READ_POINTER     0xE1
#define DS2482_WRITE_CONFIG         0xD2
#define DS2482_1WIRE_RESET          0xB4
#define DS2482_1WIRE_SINGLE_BIT     0x87
#define DS2482_1WIRE_WRITE_BYTE     0xA5
#define DS2482_1WIRE_READ_BYTE      0x96
#define DS2482_1WIRE_TRIPLET        0x78

#define POLL_LIMIT      200 // for polling 1w bus busy status

int8_t ds2482_reset(void);

struct{
    bool    busy;

    uint8_t status;
    uint8_t config;

    uint8_t i2c_error;
    uint8_t command;

    uint8_t tx_buffer[16];  // max val 9 byte len
    uint8_t rx_byte;
    uint16_t poll;          // counter
}ds2482;

typedef enum
{
  DS_OK             = 0x00U,
  DS_ERROR          = 0x01U,
  DS_BUSY           = 0x02U,
  DS_TIMEOUT        = 0x03U,
  DS_INCORECT_DATA  = 0x04U
} DS_StatusTypeDef;

static void on_status(void)
{
    ds2482.status = ds2482.rx_byte;
    ds2482.busy = false;
}

static void on_configuration(void)
{
    if(ds2482.config != ds2482.rx_byte)
        ds2482.i2c_error = DS_INCORECT_DATA;
    ds2482.busy = false;
}

static void on_data_byte(void)
{
    tsensor_on_byte(ds2482.rx_byte);
}

static void poll_1w_busy_status(void)
{
    ++ds2482.poll;
    ds2482.busy = true;
    ds2482.rx_byte = 0;
    HAL_StatusTypeDef status = HAL_I2C_Master_Receive_DMA(&HI2C, DS2482_ADDR, &ds2482.rx_byte, 1);
    if(status != HAL_OK)
        ds2482.i2c_error = status;
}

// монииторит 1wbusy
// при установленном флаге делает запрос снова
// при снятом - обновляет статус busy модуля
static int8_t on_1w_status(void)
{
    if((ds2482.rx_byte & DS_STATUS_1WB) // 1W bus busy
    && (ds2482.poll < POLL_LIMIT))
    {
        poll_1w_busy_status();
        return -1;
    }
    if(ds2482.poll >= POLL_LIMIT)
    {
        ds2482_reset();
        tsensor_on_error(TSENSOR_OUT_OF_RANGE);
        return -1;
    }
    on_status();
    return 0;
}

static int8_t set_read_pointer(void)
{
    ds2482.busy = true;
    ds2482.command = DS2482_SET_READ_POINTER;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = 0xE1;
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 2);
    if(st != HAL_OK){
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

// read data from dma
void ds2482_rx_cb(void)
{
    switch(ds2482.command)
    {
        case DS2482_DEVICE_RESET:
            on_status();
            break;
        case DS2482_WRITE_CONFIG:
            on_configuration();
            break;
        case DS2482_1WIRE_RESET:
        case DS2482_1WIRE_WRITE_BYTE:
        case DS2482_1WIRE_SINGLE_BIT:
            on_1w_status();
            break;
        case DS2482_1WIRE_READ_BYTE:
            if(!on_1w_status())
            {
                set_read_pointer();
            }
            break;
        case DS2482_SET_READ_POINTER:
            on_data_byte();
            break;
        default:
            ds2482_reset();
        // case DS2482_1WIRE_TRIPLET:
    }
}

static void recieve_status(void)
{
    ds2482.busy = true;
    ds2482.rx_byte = 0;
    HAL_StatusTypeDef status = HAL_I2C_Master_Receive_DMA(&HI2C, DS2482_ADDR, &ds2482.rx_byte, 1);
    if(status != HAL_OK)
        ds2482.i2c_error = status;
}

static void recieve_config(void)
{
    ds2482.busy = true;
    ds2482.rx_byte = 0;
    HAL_StatusTypeDef status = HAL_I2C_Master_Receive_DMA(&HI2C, DS2482_ADDR, &ds2482.rx_byte, 1);
    if(status != HAL_OK)
        ds2482.i2c_error = status;
}

static void recieve_data(void)
{
    ds2482.busy = true;
    ds2482.rx_byte = 0;
    HAL_StatusTypeDef status = HAL_I2C_Master_Receive_DMA(&HI2C, DS2482_ADDR, &ds2482.rx_byte, 1);
    if(status != HAL_OK)
        ds2482.i2c_error = status;
}

// DS2482_DEVICE_RESET         0xF0
// DS2482_SET_READ_POINTER     0xE1
// DS2482_WRITE_CONFIG         0xD2
// DS2482_1WIRE_RESET          0xB4
// DS2482_1WIRE_SINGLE_BIT     0x87
// DS2482_1WIRE_WRITE_BYTE     0xA5
// DS2482_1WIRE_READ_BYTE      0x96
// DS2482_1WIRE_TRIPLET        0x78

void ds2482_tx_cb(void)
{
    // обработчик в зависимости от однобайтной комманды
    switch(ds2482.command)
    {
        case DS2482_DEVICE_RESET:
            recieve_status();
            break;
        case DS2482_WRITE_CONFIG:
            recieve_config();
            break;
        case DS2482_1WIRE_WRITE_BYTE:
        case DS2482_1WIRE_RESET:
        case DS2482_1WIRE_READ_BYTE:
        case DS2482_1WIRE_SINGLE_BIT:
        case DS2482_1WIRE_TRIPLET:
            poll_1w_busy_status();
            break;
        case DS2482_SET_READ_POINTER: // не нужно поллить, сразу чтение
            recieve_data();
            break;
        default:
            ds2482_reset();
    }
}

int8_t ds2482_reset(void)
{
    ds2482.busy = true;
    ds2482.i2c_error = DS_OK;
    ds2482.command = DS2482_DEVICE_RESET;
    ds2482.tx_buffer[0] = ds2482.command;
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 1);
    if(st != HAL_OK){
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

int8_t ds2482_write_config(void)
{
    // PPM was removed from 11/09 revision
    // cAPU = 0x01 only if 1 slave on 1-w bus presented
    int c1WS, cSPU, cAPU;
    c1WS = false;   // 1-w speed if true - overdrive
    cSPU = false;   // strong pull up
    cAPU = false;

    ds2482.config = c1WS | cSPU | cAPU;
    ds2482.busy = true;
    ds2482.command = DS2482_WRITE_CONFIG;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = ds2482.config | (~ds2482.config << 4); // config has complementary input
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 2);
    if(st != HAL_OK){
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

int8_t ds2482_1w_reset(void)
{
    ds2482.busy = true;
    ds2482.command = DS2482_1WIRE_RESET;
    ds2482.tx_buffer[0] = ds2482.command;
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 1);
    if(st != HAL_OK){
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

int8_t ds2482_1w_write_byte(uint8_t sendbyte)
{
    ds2482.busy = true;
    ds2482.command = DS2482_1WIRE_WRITE_BYTE;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = sendbyte;
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 2);
    if(st != HAL_OK){
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

int8_t ds2482_1w_read_byte(void)
{
    ds2482.busy = true;
    ds2482.command = DS2482_1WIRE_READ_BYTE;
    ds2482.tx_buffer[0] = ds2482.command;
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 1);
    if(st != HAL_OK){
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

int8_t ds2482_1w_write_bit(uint8_t bit)
{
    ds2482.busy = true;
    ds2482.command = DS2482_1WIRE_SINGLE_BIT;
    ds2482.tx_buffer[0] = ds2482.command;
    ds2482.tx_buffer[1] = bit ? 0x80 : 0x00;
    HAL_StatusTypeDef st = HAL_I2C_Master_Transmit_DMA(&HI2C, DS2482_ADDR, ds2482.tx_buffer, 2);
    if(st != HAL_OK)
    {
        ds2482.i2c_error = st;
    }
    return (int8_t)st;
}

uint8_t ds2482_status(void)
{
    return ds2482.status;
}

bool ds2482_busy(void)
{
    return ds2482.busy;
}

int8_t ds2482_init(void)
{
    ds2482.i2c_error = DS_OK;
    ds2482.poll = 0;
    ds2482.busy = false;

    return ds2482_reset();
}

bool ds2482_is_error(void)
{
    return ds2482.i2c_error;
}
