#include <string.h>
#include "tsensor.h"
#include "stm32f4xx.h"
#include "ds2482_100.h"

#define TSENSOR_SKIP_ROM            0xCC
#define TSENSOR_CONVERT_T           0x44
#define TSENSOR_MATCH_ROM           0x55
#define TSENSOR_READ_SCRATCHPAD     0xBE
#define TSENSOR_WRITE_SCRATCHPAD    0x4E

// из главного цикла производится полинг чтения датчиков с интервалом указанным в io_control
// tsensor_read - чтение из уже сохраненных значений температур
// все датчики начинают измерение одновременно
// чтение измеренных значений происходит по-отдельности

int8_t (*cb)(uint8_t);

typedef struct
{
    uint8_t     addrs[NUM_TEMP_SENSORS * 8];    // 8-байтовые адреса
    uint16_t    val[NUM_TEMP_SENSORS];          // значение температуры
                                                // флаги ошибок, 0xF0FF опрос не производится, 0x0... - нет ошибки,
                                                                                           //  0xF001 - ошибка
                                                                                           //  0xF002 - занят
                                                                                           //  0xF003 - таймаут
    uint8_t     rx_buffer[9];                   // sensor data
    uint8_t     rx_idx;                         // побайтовый прием
    uint8_t     tx_idx;                         // побайтовая отправка
    uint8_t     current;                        // индекс текущего датчика
    uint32_t    busy_count;
}tsensor_t;
tsensor_t tsensor;

static int8_t on_error(int8_t err)
{
    // log("error %d", err);
    cb = NULL;
    return err;
}

void tsensor_on_error(tsensor_error_t error)
{
    tsensor.val[tsensor.current] = 0xF000 & error;
}

bool tsensor_driver_is_error(void)
{
    return ds2482_is_error();
}

const uint8_t crc_table[256] =
{
    0,      94,     188,    226,    97,     63,     221,    131,    194,    156,    126,    32,     163,    253,    31,     65,
    157,    195,    33,     127,    252,    162,    64,     30,     95,     1,      227,    189,    62,     96,     130,    220,
    35,     125,    159,    193,    66,     28,     254,    160,    225,    191,    93,     3,      128,    222,    60,     98,
    190,    224,    2,      92,     223,    129,    99,     61,     124,    34,     192,    158,    29,     67,     161,    255,
    70,     24,     250,    164,    39,     121,    155,    197,    132,    218,    56,     102,    229,    187,    89,     7,
    219,    133,    103,    57,     186,    228,    6,      88,     25,     71,     165,    251,    120,    38,     196,    154,
    101,    59,     217,    135,    4,      90,     184,    230,    167,    249,    27,     69,     198,    152,    122,    36,
    248,    166,    68,     26,     153,    199,    37,     123,    58,     100,    134,    216,    91,     5,      231,    185,
    140,    210,    48,     110,    237,    179,    81,     15,     78,     16,     242,    172,    47,     113,    147,    205,
    17,     79,     173,    243,    112,    46,     204,    146,    211,    141,    111,    49,     178,    236,    14,     80,
    175,    241,    19,     77,     206,    144,    114,    44,     109,    51,     209,    143,    12,     82,     176,    238,
    50,     108,    142,    208,    83,     13,     239,    177,    240,    174,    76,     18,     145,    207,    45,     115,
    202,    148,    118,    40,     171,    245,    23,     73,     8,      86,     180,    234,    105,    55,     213,    139,
    87,     9,      235,    181,    54,     104,    138,    212,    149,    203,    41,     119,    244,    170,    72,     22,
    233,    183,    85,     11,     136,    214,    52,     106,    43,     117,    151,    201,    74,     20,     246,    168,
    116,    42,     200,    150,    21,     75,     169,    247,    182,    232,    10,     84,     215,    137,    107,    53
};

static bool check_CRC(const uint8_t *buf, size_t bufSize)
{
    uint8_t crc = 0;
    for(size_t ii = 0; ii < bufSize; ii++)
    {
        crc = crc_table[crc ^ buf[ii]];
    }
    return crc == buf[bufSize];
}

uint8_t* tsensor_addrs(void)
{
    return tsensor.addrs;
}

void tsensor_set_addrs(uint8_t* message)
{
    memcpy(tsensor.addrs, message, NUM_TEMP_SENSORS * 8);
}

// для начала измерения температуры всех термодатчиков используется SKIP ROM
// --------------------------------------------------------------------------
//     TX Reset Reset pulse (480-960 μs).
//     RX Presence Presence pulse.
//     TX CCh Issue “Skip ROM” command.
//     TX 44h Issue “Convert T” command.
//     TX <I/O LINE HIGH> I/O line is held high for at least a period of time greater
//         than tconv by bus master to allow conversion to complete.

//  oooooooo8 ooooooooooo   o      oooooooooo  ooooooooooo        oooooooo8   ooooooo  oooo   oooo ooooo  oooo ooooooooooo oooooooooo  ooooooooooo
// 888        88  888  88  888      888    888 88  888  88      o888     88 o888   888o 8888o  88   888    88   888    88   888    888 88  888  88
//  888oooooo     888     8  88     888oooo88      888          888         888     888 88 888o88    888  88    888ooo8     888oooo88      888
//         888    888    8oooo88    888  88o       888          888o     oo 888o   o888 88   8888     88888     888    oo   888  88o       888
// o88oooo888    o888o o88o  o888o o888o  88o8    o888o          888oooo88    88ooo88  o88o    88      888     o888ooo8888 o888o  88o8    o888o

// 1w write из доки case B
static int8_t send_convert_temp(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        int8_t ret = ds2482_1w_write_byte(TSENSOR_CONVERT_T);
        if(ret)
            return on_error(ret);
        cb = NULL;
    }
    return 0;
}

static int8_t send_skip_rom(uint8_t status)
{
    if((!status & DS_STATUS_1WB) && (status & DS_STATUS_PPD))
    {
        int8_t ret = ds2482_1w_write_byte(TSENSOR_SKIP_ROM);
        if(ret)
            return on_error(ret);
        cb = send_convert_temp;
    }
    return 0;
}

int8_t tsensor_start_convesion(void)
{
    int8_t ret = ds2482_1w_reset();
    if(ret) // error
        return on_error(ret);
    cb = send_skip_rom;
    return 0;
}

// oooooooooo  ooooooooooo      o      ooooooooo
//  888    888  888    88      888      888    88o
//  888oooo88   888ooo8       8  88     888    888
//  888  88o    888    oo    8oooo88    888    888
// o888o  88o8 o888ooo8888 o88o  o888o o888ooo88

// для чтения используется MATCH ROM
// ---------------------------------
//     TX Reset Reset pulse.
//     RX Presence Presence pulse.
//     TX 55h Issue “Match ROM” command.
//     TX <64-bit ROM code> Issue address for DS18B20.
//     TX BEh Issue “Read Scratchpad” command.
//     RX <9 data bytes> Read entire scratchpad plus CRC; the master now
//         recalculates the CRC of the eight data bytes received
//         from the scratchpad, compares the CRC calculated and
//         the CRC read. If they match, the master continues; if
//         not, this read operation is repeated.
//     TX Reset Reset pulse.
//     RX Presence Presence pulse, done.

// прием идет 9 байт
void tsensor_on_byte(uint8_t byte)
{
    tsensor.rx_buffer[tsensor.rx_idx] = byte;
    ++tsensor.rx_idx;
    if(tsensor.rx_idx == 9)
    {
        cb = NULL;
        if(check_CRC(tsensor.rx_buffer, 8))
        {
            int16_t temperature = tsensor.rx_buffer[0] | tsensor.rx_buffer[1] << 8;
            // (t > -55_C) && (t < 125_C)
            if((temperature > TSENSOR_MIN_VALUE) && (temperature < TSENSOR_MAX_VALUE))
            {
                tsensor.val[tsensor.current] = temperature >> 4;
            }else{
                tsensor_on_error(TSENSOR_OUT_OF_RANGE);
            }
        }else{
            tsensor_on_error(TSENSOR_CRC_INCORRECT);
        }
        tsensor.rx_idx = 0;
        tsensor_next(); // set next index here
    }
}

static int8_t read_byte(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        int8_t ret = ds2482_1w_read_byte();
        if(ret)
            return on_error(ret);
    }
    return 0;
}

static int8_t send_read_scratchpad(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        int8_t ret = ds2482_1w_write_byte(TSENSOR_READ_SCRATCHPAD);
        if(ret)
            return on_error(ret);
        cb = read_byte;
    }
    return 0;
}

static int8_t send_tsensor_addr(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        int8_t ret = ds2482_1w_write_byte(tsensor.addrs[tsensor.current * 8 + tsensor.tx_idx]);
        if(ret)
            return on_error(ret);
        ++tsensor.tx_idx;
        if(tsensor.tx_idx == 8)
        {
            cb = send_read_scratchpad;
            tsensor.tx_idx = 0;
        }
    }
    return 0;
}

static int8_t send_match_rom(uint8_t status)
{
    if(!(status & DS_STATUS_1WB) && (status & DS_STATUS_PPD))
    {
        int8_t ret = ds2482_1w_write_byte(TSENSOR_MATCH_ROM);
        if(ret)
            return on_error(ret);
        tsensor.tx_idx = 0;
        cb = send_tsensor_addr;
    }
    return 0;
}

static int8_t send_1w_reset(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        int8_t ret = ds2482_1w_reset();
        if(ret)
            return on_error(ret);
        cb = NULL;
    }
    return 0;
}

static int8_t send_write_config(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        uint8_t byte;
        if(tsensor.tx_idx == 0)
        {
            byte = 0x1F;
        }else{
            byte = 0;
        }
        ++tsensor.tx_idx;
        int8_t ret = ds2482_1w_write_byte(byte); // set 9 bit resolution
        if(ret)
        {
            tsensor.tx_idx = 0;
            return on_error(ret);
        }
        if(tsensor.tx_idx == 3)
            cb = send_1w_reset;
    }
    return 0;
}

static int8_t send_write_scratchpad(uint8_t status)
{
    if(!status & DS_STATUS_1WB)
    {
        int8_t ret = ds2482_1w_write_byte(TSENSOR_WRITE_SCRATCHPAD);
        if(ret)
            return on_error(ret);
        tsensor.tx_idx = 0;
        cb = send_write_config;
    }
    return 0;
}

int8_t tsensor_index(void)
{
    return tsensor.current;
}

int8_t tsensor_next(void)
{
    ++tsensor.current;
    tsensor.current = tsensor.current % NUM_TEMP_SENSORS;
    return tsensor.current;
}

int8_t tsensor_read(void)
{
    int8_t ret = ds2482_1w_reset();
    if(ret) // error
        return on_error(ret);
    cb = send_match_rom;

    return 0;
}

int8_t tsensor_poll(void)
{
    if(!ds2482_busy())
    {
        tsensor.busy_count = 0;
        uint8_t status = ds2482_status();
        if(cb)
            return cb(status);
    }else{
        ++tsensor.busy_count;
        if(tsensor.busy_count >= TSENSOR_MAX_BUSY)
        {
            tsensor.busy_count = 0;
            return -1;
        }
    }
    return 0;
}

// get saved value from buffer
uint16_t tsensor_value(uint8_t idx)
{
    return tsensor.val[idx];
}

int8_t tsensors_reset(void)
{
    int8_t ret = ds2482_reset();
    if(ret) // error
        return on_error(ret);
    return 0;
}

void tsensors_init(void)
{
    memset(tsensor.rx_buffer, 0, sizeof(tsensor.rx_buffer));
    memset(tsensor.val, 0, sizeof(tsensor.val));
    tsensor.busy_count = 0;
    tsensor.current = 0;
    tsensor.rx_idx = 0;
    tsensor.tx_idx = 0;
    ds2482_init();
    cb = send_write_scratchpad;
}
