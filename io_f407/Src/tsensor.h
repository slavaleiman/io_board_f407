// this code made for DS18D20 temperature sensor

#ifndef _TSENSOR_H_
#define _TSENSOR_H_

#include "stm32f4xx.h"
#include <stdbool.h>

#define NUM_TEMP_SENSORS    24
#define TSENSOR_MAX_BUSY    500

#define TSENSOR_MIN_VALUE   -55 * 16
#define TSENSOR_MAX_VALUE   125 * 16

typedef enum
{
    TSENSOR_NO_RESPONSE = 0,
    TSENSOR_CRC_INCORRECT,
    TSENSOR_OUT_OF_RANGE
} tsensor_error_t;

bool        tsensors_flag(uint8_t idx);
void        tsensors_init(void);
int8_t      tsensor_start_convesion(void);
int8_t      tsensor_read(void);
int8_t      tsensor_index(void);
int8_t      tsensor_next(void);
uint16_t    tsensor_value(uint8_t idx); // get saved value
uint8_t*    tsensor_addrs(void);
void        tsensor_set_addrs(uint8_t* message);
void        tsensor_on_byte(uint8_t);
int8_t      tsensor_poll(void);
int8_t      tsensors_reset(void);
void        tsensor_on_error(tsensor_error_t);
bool        tsensor_driver_is_error(void);

#endif
