
#include "stm32f4xx.h"
#include "io_control.h"
#include <string.h>
#include <stdbool.h>
#include "stm32f4xx_hal_uart.h"
#include "modbus.h"
#include "tsensor.h"
#include "i2c_reset_erratum.h"

extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern I2C_HandleTypeDef hi2c1;

#define HI2C                hi2c1
#define MAX_INPUT_LEN       16
#define RXBUFFERSIZE        16
#define NZ_DEFAULT          4   // количество выборок при считывании порта
#define IMPULSE_BUFFER_SIZE 5

struct
{
    bool        update_tsensors;
    bool        update_idr;

    uint16_t    idr_buffer[MAX_INPUT_LEN];  // input data register
    uint8_t     num_samples;                // количество выборок по которым производится усреднение
    uint8_t     input_buff_idx;             // текущий индекс в дребезг буфере
    uint16_t    tsensor_poll_period;
    uint8_t     rx_buffer[RXBUFFERSIZE];    // receive from sensor

    uint16_t    pollp; // упакованные параметры опроса порта и датчиков

    struct
    {
        uint32_t    counter;    // global impulse counter
        uint16_t    buffer[IMPULSE_BUFFER_SIZE];  // for filtering
        uint8_t     idx;
        uint32_t    freq;       // approximated value * 10
    }impulse;

}io_control;

extern UART_HandleTypeDef huart1;

static void set_port_period(uint16_t port_period)
{
    HAL_TIM_Base_Stop_IT(&htim6);
    if((port_period > 0) && (port_period <= 1020))
    {
        htim6.Init.Period = port_period;
        if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
        {
            _Error_Handler(__FILE__, __LINE__);
        }
        HAL_TIM_Base_Start_IT(&htim6);
    }
}

static void set_port_buff_len(uint8_t nz)
{
    io_control.num_samples = nz;
}

static void set_term_poll_period(uint16_t t_period)
{
    io_control.tsensor_poll_period = t_period;
    HAL_TIM_Base_Stop_IT(&htim7);
    if((t_period > 0) && (t_period <= 1000))
    {
        htim7.Init.Period = t_period;
        if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
        {
            _Error_Handler(__FILE__, __LINE__);
        }
        HAL_TIM_Base_Start_IT(&htim7);
    }
}

static void clear_impulse_counter(void)
{
    io_control.impulse.counter = 0;
}

uint32_t io_control_impulse_counter(void)
{
    return io_control.impulse.counter;
}

uint32_t io_control_impulse_freq(void)
{
    // S = (s[i] + s[i - 1] + s[i - 2] + s[i - 3] + s[i - 4]) / 8
    // чтобы перевести в герцы, нужно умножить на 4, так как здесь количество импульсов за 250 мс
    // по протоколу нужно присылать удесятереное значение
    // => / 8 * 4 * 10 => * 5
    uint32_t value = (io_control.impulse.buffer[(io_control.impulse.idx + IMPULSE_BUFFER_SIZE - 1) % IMPULSE_BUFFER_SIZE] // - 1 + 5 = 4
                + 2 * io_control.impulse.buffer[(io_control.impulse.idx + IMPULSE_BUFFER_SIZE - 2) % IMPULSE_BUFFER_SIZE]
                + 2 * io_control.impulse.buffer[(io_control.impulse.idx + IMPULSE_BUFFER_SIZE - 3) % IMPULSE_BUFFER_SIZE]
                + 2 * io_control.impulse.buffer[(io_control.impulse.idx + IMPULSE_BUFFER_SIZE - 4) % IMPULSE_BUFFER_SIZE]
                    + io_control.impulse.buffer[(io_control.impulse.idx + IMPULSE_BUFFER_SIZE - 5) % IMPULSE_BUFFER_SIZE]) * 5;
    io_control.impulse.freq = value;
    return io_control.impulse.freq;
}

void io_control_on_impulse(uint16_t imp)
{
    io_control.impulse.counter += imp;
    io_control.impulse.buffer[io_control.impulse.idx] = imp; // импульсов в секунду
    io_control.impulse.idx = (io_control.impulse.idx + 1) % 5;
}

static void set_poll_params(uint16_t cnr)   // установка параметров опроса
{
    uint16_t port_period = (cnr & 0xFF) * 4;    // интервал опроса порта входа, мс
    set_port_period(port_period);

    uint8_t nz = (cnr & 0xF00) >> 8;            // количество выборок для усреднения
    set_port_buff_len(nz);

    uint16_t t_period = (((cnr & 0x7000) >> 12) + 1) * 250;
    set_term_poll_period(t_period);

    if(cnr & 8000)
        clear_impulse_counter();
    io_control.pollp = cnr;
}

static void write_flash(uint8_t* data)
{
    HAL_FLASH_Unlock(); // unlock flash writing
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR);
    do{
        FLASH_Erase_Sector(DATA_SECTOR, FLASH_VOLTAGE_RANGE_3);
        HAL_StatusTypeDef status = HAL_OK;
        for (uint32_t i = 0; i < FLASH_DATA_SIZE; ++i)
        {
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, CONFIG_DATA_BEGIN + i, data[i]);
            if(status != HAL_OK)
                break;
        }

    }while(0);

    HAL_FLASH_Lock(); // lock the flash
}

static void read_flash(uint8_t* data, uint32_t size)
{
    uint32_t i;
    for(i = 0; i < size; ++i)
        data[i] = *(uint8_t*)(CONFIG_DATA_BEGIN + i);
}

// и тут и там происходит запись одинаковых данных, так как перезапись происходит секторами
void io_control_save_data(void)
{
    uint8_t data[FLASH_DATA_SIZE];
    data[0] = io_control.pollp & 0xFF;
    data[1] = io_control.pollp >> 8;
    uint8_t* addrs = tsensor_addrs();
    memcpy(&data[2], addrs, FLASH_DATA_SIZE - 2);
    write_flash(data);
}

// установка параметров опроса с проверкой и сохранением во флеш
void io_control_set_poll_params(uint16_t cnr)
{
    // сравнить с текущим значением и при различии записать во флеш
    if(cnr != io_control.pollp)
    {
        set_poll_params(cnr);
    }
}

uint16_t io_control_poll_params(void) // установка параметров опроса
{
    return io_control.pollp;
}

void io_control_poll_params_init(uint16_t pollp)
{
    if(!pollp) // при отсутствии сохраненных параметров
    {
        io_control.pollp = 0;
        uint16_t port_period = htim6.Init.Period;
        io_control.pollp |= (port_period >> 2) & 0xFF; // интервал опроса порта входа, мс
        set_port_period(port_period);

        io_control.pollp |= (NZ_DEFAULT << 8) & 0xF00; // количество выборок для усреднения
        set_port_buff_len(NZ_DEFAULT);

        uint16_t t_period = htim7.Init.Period;
        io_control.pollp |= (((t_period / 250) - 1) << 12) & 0x7000;
        set_term_poll_period(t_period);
    }else{
        io_control.pollp = pollp;
        set_poll_params(pollp);
    }
    clear_impulse_counter();
}

uint16_t io_control_idr(void)
{
    uint16_t idr = 0;
    for(uint8_t n = 0; n < io_control.num_samples; ++n)
    {
        idr &= !io_control.idr_buffer[n];
    }
    return idr;
}

void io_control_set_gpio_output(uint16_t output)
{
    OUTPUT_PORT->ODR = output;
}

void io_control_update_idr(void)
{
    io_control.update_idr = true;
}

void io_control_poll(void)
{
    if(io_control.update_idr)
    {
        io_control.idr_buffer[io_control.input_buff_idx] = INPUT_PORT->IDR;
        io_control.input_buff_idx = (io_control.input_buff_idx + 1) % MAX_INPUT_LEN;
        io_control.update_idr = false;
    }

    if(tsensor_poll())
    {
        if(tsensors_reset())
        {
            // I2C_ClearBusyFlagErratum();
            return;
        }
    }

    if(io_control.update_tsensors && HAL_I2C_GetState(&HI2C) == HAL_I2C_STATE_READY)
    {
        tsensor_start_convesion();
        io_control.update_tsensors = false;
    }

    // if(tsensor_index() < NUM_TEMP_SENSORS) // при условии что модуль не занят
    // {
    //     tsensor_read();
    // }
}

bool io_control_tsensors_status(void)
{
    if(io_control.tsensor_poll_period == 0)
        return 0xFF;
    return tsensor_driver_is_error();
}

uint8_t* io_control_tsensor_addrs(void)
{
    return tsensor_addrs();
}

uint16_t compare_buffers(uint8_t* message, uint8_t* addrs, uint16_t size)
{
    for(uint16_t i = 0; i < size; ++i)
    {
        if(message[i] != addrs[i])
            return i + 1;
    }
    return 0;
}

void io_control_tsensor_set_addrs(uint8_t* message)
{
    uint8_t* addrs = tsensor_addrs();
    if(compare_buffers(message, addrs, FLASH_DATA_SIZE))
    {
        tsensor_set_addrs(message);
    }
}

void io_control_update_tsensors(void)
{
    io_control.update_tsensors = true;
}

uint16_t io_control_tsensor(uint8_t idx)
{
    uint8_t packed_poll_period = (io_control.pollp & 0x7000) >> 12;
    if(packed_poll_period <= 3)
        return tsensor_value(idx); // value or error flag in 16 bit
    return 0xF000; // if no tsensors polling
}

void io_control_load_rom_data(void)
{
    uint8_t io_control_params[FLASH_DATA_SIZE]; // pop + 24 *addr = 2 + 24 * 8
    memset(io_control_params, 0, FLASH_DATA_SIZE);

    read_flash(io_control_params, FLASH_DATA_SIZE);

    uint16_t pollp = io_control_params[0] | io_control_params[1] << 8; // LSB first
    io_control_poll_params_init(pollp);

    tsensor_set_addrs(&io_control_params[2]);
}

void io_control_init(void)
{
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    // GPIOB - input
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = GPIO_PIN_All & ~(GPIO_PIN_6 | GPIO_PIN_7); // USED FOR I2C
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    // GPIOC - output
    GPIO_InitStruct.Pin = GPIO_PIN_All;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    OUTPUT_PORT->ODR = 0;
    memset(io_control.idr_buffer, 0, sizeof(io_control.idr_buffer));
    io_control.input_buff_idx = 0;

    memset(io_control.impulse.buffer, 0, sizeof(io_control.impulse.buffer));
    io_control.impulse.idx = 0;

    modbus_init();
    tsensors_init();

    io_control_load_rom_data();

    // init timer for get pinout status
    HAL_TIM_Base_Start_IT(&htim3);
    HAL_TIM_Base_Start_IT(&htim6);
    HAL_TIM_Base_Start_IT(&htim7);
}
