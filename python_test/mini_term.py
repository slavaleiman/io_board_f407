import time
import serial
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s " + sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1])

def pack_command(values):
    for i in values:
        string += struct.pack('!B', i)

# input_string = 1
while 1 :
    if(ser.isOpen() == False):
        ser.open()

    input_string = input(">> ")

    if input_string == 'exit':
        ser.close()
        exit()
    else:
        ser.write(str.encode(input_string))
        out = ''

        seq = []
        time.sleep(1)
        while ser.inWaiting() > 0:
            for c in ser.read():
                seq.append(chr(c)) #convert from ANSII
                out = ''.join(str(v) for v in seq) #Make a string from array
                if chr(c) == '\n':
                    break

        if out != '':
            print (">>" + out)