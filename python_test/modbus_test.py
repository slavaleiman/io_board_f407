
import time
import serial
import sys
import struct
import math
import subprocess
import serial
import time
from time import sleep
import struct
import sys

if len(sys.argv) < 2:
    print("USAGE: %s " + sys.argv[0])
    sys.exit(1)

ser = serial.Serial(port=sys.argv[1],
                    baudrate=19200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS)

def calc(data):
    crc_table=[ 0x0000,0xC0C1,0xC181,0x0140,0xC301,0x03C0,0x0280,0xC241,
                0xC601,0x06C0,0x0780,0xC741,0x0500,0xC5C1,0xC481,0x0440,
                0xCC01,0x0CC0,0x0D80,0xCD41,0x0F00,0xCFC1,0xCE81,0x0E40,
                0x0A00,0xCAC1,0xCB81,0x0B40,0xC901,0x09C0,0x0880,0xC841,
                0xD801,0x18C0,0x1980,0xD941,0x1B00,0xDBC1,0xDA81,0x1A40,
                0x1E00,0xDEC1,0xDF81,0x1F40,0xDD01,0x1DC0,0x1C80,0xDC41,
                0x1400,0xD4C1,0xD581,0x1540,0xD701,0x17C0,0x1680,0xD641,
                0xD201,0x12C0,0x1380,0xD341,0x1100,0xD1C1,0xD081,0x1040,
                0xF001,0x30C0,0x3180,0xF141,0x3300,0xF3C1,0xF281,0x3240,
                0x3600,0xF6C1,0xF781,0x3740,0xF501,0x35C0,0x3480,0xF441,
                0x3C00,0xFCC1,0xFD81,0x3D40,0xFF01,0x3FC0,0x3E80,0xFE41,
                0xFA01,0x3AC0,0x3B80,0xFB41,0x3900,0xF9C1,0xF881,0x3840,
                0x2800,0xE8C1,0xE981,0x2940,0xEB01,0x2BC0,0x2A80,0xEA41,
                0xEE01,0x2EC0,0x2F80,0xEF41,0x2D00,0xEDC1,0xEC81,0x2C40,
                0xE401,0x24C0,0x2580,0xE541,0x2700,0xE7C1,0xE681,0x2640,
                0x2200,0xE2C1,0xE381,0x2340,0xE101,0x21C0,0x2080,0xE041,
                0xA001,0x60C0,0x6180,0xA141,0x6300,0xA3C1,0xA281,0x6240,
                0x6600,0xA6C1,0xA781,0x6740,0xA501,0x65C0,0x6480,0xA441,
                0x6C00,0xACC1,0xAD81,0x6D40,0xAF01,0x6FC0,0x6E80,0xAE41,
                0xAA01,0x6AC0,0x6B80,0xAB41,0x6900,0xA9C1,0xA881,0x6840,
                0x7800,0xB8C1,0xB981,0x7940,0xBB01,0x7BC0,0x7A80,0xBA41,
                0xBE01,0x7EC0,0x7F80,0xBF41,0x7D00,0xBDC1,0xBC81,0x7C40,
                0xB401,0x74C0,0x7580,0xB541,0x7700,0xB7C1,0xB681,0x7640,
                0x7200,0xB2C1,0xB381,0x7340,0xB101,0x71C0,0x7080,0xB041,
                0x5000,0x90C1,0x9181,0x5140,0x9301,0x53C0,0x5280,0x9241,
                0x9601,0x56C0,0x5780,0x9741,0x5500,0x95C1,0x9481,0x5440,
                0x9C01,0x5CC0,0x5D80,0x9D41,0x5F00,0x9FC1,0x9E81,0x5E40,
                0x5A00,0x9AC1,0x9B81,0x5B40,0x9901,0x59C0,0x5880,0x9841,
                0x8801,0x48C0,0x4980,0x8941,0x4B00,0x8BC1,0x8A81,0x4A40,
                0x4E00,0x8EC1,0x8F81,0x4F40,0x8D01,0x4DC0,0x4C80,0x8C41,
                0x4400,0x84C1,0x8581,0x4540,0x8701,0x47C0,0x4680,0x8641,
                0x8201,0x42C0,0x4380,0x8341,0x4100,0x81C1,0x8081,0x4040]

    crc_hi=0xFF
    crc_lo=0xFF

    for w in data:
        # index=crc_lo ^ ord(w)
        index=crc_lo ^ w
        crc_val=crc_table[index]
        crc_temp=int(crc_val/256)
        crc_val_low=crc_val-(crc_temp*256)
        crc_lo=crc_val_low ^ crc_hi
        crc_hi=crc_temp

    crc=crc_hi*256 +crc_lo
    return crc

def print_green(line):
    print('\x1b\033[92m' + line + '\x1b[0m\n')

def print_red(line):
    print('\x1b\033[91m' + line + '\x1b[0m\n')

def process_cmd(command):
    if(ser.isOpen() == False):
        ser.open()

    cmd_bytes = bytearray.fromhex(command)

    crc = calc(cmd_bytes)
    crc_hi = crc >> 8
    crc_lo = crc & 0xFF
    cmd_bytes.append(crc_lo)
    cmd_bytes.append(crc_hi)
    print("send:       " + command + " %02X %02X" % (crc_lo, crc_hi))

    ser.write(cmd_bytes)
    out = ''
    seq = []
    time.sleep(1)
    while ser.inWaiting() > 0:
        for c in ser.read():
            seq.append(c)
            out = ''.join(("%02X " % v) for v in seq)
            if chr(c) == '\n':
                break
    return str(out)

def test_modbus(command, is_valid=1):

    recieved = process_cmd(command)

    if len(recieved) > 2:
        print("  recieved: %s" % recieved)

        is_err = int((recieved[3]), 16) & 0x8
        # print("  is_error: %d" % is_err)

        if is_valid > 1:
            print_red('\x1b\033[91m' + 'NOT EXPECTED RESULT!' + '\x1b[0m')
            return

        if ((not is_err and is_valid == 1) or (is_err and not (is_valid == 1))):
            print_green('\x1b\033[92m' + 'OK' + '\x1b[0m')
        else:
            print_red('\x1b\033[91m' + 'RESULT INCORRECT!' + '\x1b[0m')
    else:
        if(is_valid == 2):
            print_green('\x1b\033[92m' + 'NO RESULT CORRECT!' + '\x1b[0m')
        else:
            print_red('\x1b\033[91m' + 'NO RESULT RECIEVED' + '\x1b[0m')


echo_function =         "01 08 00 00 00 00"
echo_function_2 =       "01 08 00 00 00 00 00"  # invalid len

read_term_addrs =       "01 03 FE FF 00 60"
read_term_addrs_2 =     "01 03 FE FF 00 60 00"  # invalid len
read_poll_params =      "01 03 FF FF 00 01"
read_poll_params_2 =    "01 03 FF FF 00 01 00"  # invalid len

get_IDR =               "01 04 FF FF 00 05"
get_IDR_2 =             "01 04 FF FF 00 05 00"  # invalid len

get_term = [
    "01 04 FE FF 00 00",    # invalid
    "01 04 FE FF 00 01",    # valid
    "01 04 FE FF 00 06",    # valid
    "01 04 FE FF 00 19",    # valid
    "01 04 FE FF 00 FF",     # invalid, must return error
    "01 04 FE FF 00 19 00", # invalid len
]
set_output_pins = [
    "01 06 FD FF 00 00",    # valid, set all 0
    "01 06 00 00 00 00",    # invalid
    "01 06 FD FF 00 FF",     # valid, set all 1
    "01 06 FD FF 00 FF 00"  # invalid len
]
set_poll_params = [
    "01 10 FF FF 11 11 00",
    "01 10 FF FF 22 22 00 00", # invalid len
    "01 10 FF FF 33 33 00",      # valid
    "01 10 FF FF FF FF 00 00 00" # invalid len
]
set_term_addrs = "01 10 FE FF 00 60 C0 01 00 00 00 00 00 00 00 00 02 00 00 00 00 00 00 00 00 03 00 00 00 00 00 00 00 00 04 00 00 00 00 00 00 00 00 05 00 00 00 00 00 00 00 00 06 00 00 00 00 00 00 00 00 07 00 00 00 00 00 00 00 00 08 00 00 00 00 00 00 09 00 00 00 00 00 00 10 00 00 00 00 00 00 11 00 00 00 00 00 00 12 00 00 00 00 00 00 13 00 00 00 00 00 00 14 00 00 00 00 00 00 15 00 00 00 00 00 00 00 00 16 00 00 00 00 00 00 00 00 17 00 00 00 00 00 00 00 00 18 00 00 00 00 00 00 00 00 19 00 00 00 00 00 00 00 00 20 00 00 00 00 00 00 00 00 21 00 00 00 00 00 00 00 00 22 00 00 00 00 00 00 23 00 00 00 00 00 00 24 00 00"

EXPECT_ERROR    = 0  # ожидаем ответ в ошибкой
EXPECT_OK       = 1  # ожидаем корректный ответ
EXPECT_NORESP   = 2  # ожидаем отсутствие ответа

def modbus_start_test():
    test_modbus(echo_function,      EXPECT_OK)
    test_modbus(echo_function_2,    EXPECT_NORESP)
    test_modbus(read_sensors,       EXPECT_OK)
    test_modbus(read_sensors_2,     EXPECT_NORESP)
    test_modbus(read_poll_params,   EXPECT_OK)
    test_modbus(read_poll_params_2, EXPECT_NORESP)
    test_modbus(get_IDR,            EXPECT_OK)
    test_modbus(get_IDR_2,          EXPECT_NORESP)

    test_modbus(get_term[0],        EXPECT_ERROR)
    test_modbus(get_term[1],        EXPECT_OK)
    test_modbus(get_term[2],        EXPECT_OK)
    test_modbus(get_term[3],        EXPECT_OK)
    test_modbus(get_term[4],        EXPECT_ERROR)
    test_modbus(get_term[5],        EXPECT_NORESP)

    test_modbus(set_output_pins[0],     EXPECT_OK)
    test_modbus(set_output_pins[1],     EXPECT_ERROR)
    test_modbus(set_output_pins[2],     EXPECT_OK)
    test_modbus(set_output_pins[3],     EXPECT_NORESP)

    test_modbus(set_poll_params[0],    EXPECT_OK)
    test_modbus(set_poll_params[1],    EXPECT_NORESP)
    test_modbus(set_poll_params[2],    EXPECT_OK)
    test_modbus(set_poll_params[3],    EXPECT_NORESP)

def get_term_addr():
    res2 = process_cmd(read_term_addrs)
    if(res2):
        print("recieved:   " + res2)
        print_green("OK")
    else:
        print_red("NO RESULT")

# set addresses and read back and check
def set_term_addr():
    res = process_cmd(set_term_addrs)
    if(res):
        print("recieved: " + res)
        print_green("OK")
    else:
        print_red("NO RESULT")

def poll_params_test():
    result = process_cmd(set_poll_params[0])

# ----------------------------------- START TESTS -----------------------------------------
# modbus_start_test()
# set_term_addr()
get_term_addr()
